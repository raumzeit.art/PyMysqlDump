import DumpHelper as dh
import subprocess
import os
import math
from os import path
from threading import Thread
import pathlib

class Mysql2Dump(dh.DumpHelperBase):

	def run(self):
		self.__oldDumpFiles = set()
		self._clearMysqlDumpFolder()
		self._createDumpProcessList()
		self._handleMysqlDumpProcesses()
		
	def _addScriptParmeters(self, cmdParser):
		mysql2DumpCmdLineOptions = cmdParser.add_argument_group("mysql2dump options")
		self._addCmdlArg(mysql2DumpCmdLineOptions, "mysql2dump", "-r", "--replacer", choices=['wp-cli', 'intern'], default="intern", help="Set the replacement algorithm, which should be used for automatically host adjustmenst")
		newLineParserGroup = mysql2DumpCmdLineOptions.add_mutually_exclusive_group()
		newLineParserGroup.add_argument('-n', '--newline', action='store_true', default=True, help="Convert single line INSERT commands in the mysqldump output into multiline statements. Will be ignored in combination with the --tab option.")
		newLineParserGroup.add_argument('-N', '--no_newline', action='store_false', default=True, dest='newline', help="Do not convert single line INSERT commands in the mysqldump output into multiline statements. Will be ignored in combination with the --tab option.")

		indentionParserGroup = mysql2DumpCmdLineOptions.add_mutually_exclusive_group()
		indentionParserGroup.add_argument('-i', '--indention', action='store_true', default=True, help="Add an idention to each new line inside a INSERT command. Will be ignored in combination with the --tab option.")
		indentionParserGroup.add_argument('-I', '--no_indention', action='store_false', default=True, dest="indention", help="Do not add an idention to each new line inside a INSERT command. Will be ignored in combination with the --tab option.")

	def __enter__(self):
		super().__enter__()
		self._tableCur = self._connection.cursor(buffered=True)
		tableSQL = "SHOW TABLES"
		self._tableCur.execute(tableSQL)
		if (self._cmdArgs["replacer"] == "wp-cli"):
			self._wp_local2RemoteHost()
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self._tableCur.close()
		if (self._cmdArgs["verbose"] >= 1):
			newDumpfiles = set(self._get_sql_dump_files())
			dumpFilesDiff = self.__oldDumpFiles.difference(newDumpfiles)
			if dumpFilesDiff:
				print("The following dump files were removed:\n  - {}".format("\n  - ".join(dumpFilesDiff)))
		if (self._cmdArgs["replacer"] == "wp-cli"):
			self._wp_remote2LocalHost()
		else:
			self._intern_local2RemoteHost()
		super().__exit__(exc_type, exc_val, exc_tb)

	def _clearMysqlDumpFolder(self):
		for filename, file in self._get_sql_dump_files(True):
			self.__oldDumpFiles.add(filename)
			os.remove(file)

	def _createSubprocess(self, mysqlDumpArgs, tableName):
		if self._cmdArgs['tab']:
			return (subprocess.Popen(" ".join(mysqlDumpArgs), shell=True), tableName)
		else:
			return (self._popen(*mysqlDumpArgs), tableName)

	def _createDumpProcessList(self):
		self._mysqlDumpProcessList = []

		for (tableName,) in self._tableCur:
			mysqlDumpArgs = [
				"mysqldump", 
				"--login-path={}".format(self._cmdArgs["login_path"]) if self._checkArg("login_path") else "",
				"--add-drop-table",
				"--skip-dump-date"
			]
			if self._cmdArgs['tab']:
				mysqlDumpArgs.extend(
					(
						"--fields-terminated-by=~@~",
						"--tab={}".format(self._cmdArgs["location"])
					)
				)
			if not self._cmdArgs['newline']:
				mysqlDumpArgs.append(
					"--result-file={}".format(path.join(self._cmdArgs["location"], "{}.sql".format(tableName)))
				)
			mysqlDumpArgs.extend(
				(self.database, tableName)
			)
			self._mysqlDumpProcessList.append(self._createSubprocess(mysqlDumpArgs, tableName))

	def _handleMysqlDumpProcesses(self):
		if self._cmdArgs['tab']:
			for (prcs, tableName) in self._mysqlDumpProcessList:
				prcs.wait()
		else:
			nrOfProcesses = len(self._mysqlDumpProcessList)
			j_max = math.ceil(nrOfProcesses/self._cmdArgs["threads"])
			for i in range(0, self._cmdArgs["threads"]):
				t = Thread(target = self._dumpPostProcessing, args=(i, nrOfProcesses, j_max, self._mysqlDumpProcessList))
				t.start()
				self._threadList.append(t)
			self._verbosePrint("Dump {0} tables from database `{database}`".format(nrOfProcesses, **self._cmdArgs))
			self._waitForThreads()

	def _dumpPostProcessing(self, i, i_max, j_max, mysqlDumpProcessList):
		for j in range(0, j_max):
			index = j + i*j_max
			if index >= i_max:
				return
			procHandler, tableName = mysqlDumpProcessList[index]
			dumpStdout, dumpStderr = procHandler.communicate()

			if (procHandler.returncode > 0):
				print(
					"Unable to dump the table {}: {}".format(
						tableName,
						dumpStderr.decode()
					)
				)
				self._returnCode = dh.DUMPING_TABLE_FAILED
			else:
				if self._cmdArgs['newline']:
					self._writeDumpToFile(
						dumpStdout.decode(),
						path.join(self._cmdArgs["location"], "{}.sql".format(tableName))
					)
				
	def _writeDumpToFile(self, dumpStr, sqlFile):
		if self._cmdArgs['indention']:
			self._indention = "  "
		else:
			self._indention = ""

		dumpLines = (
			lines.rstrip() + "\n" for subItems in map(
				lambda line : self._separateInsertIntoMultilines(line) if line.lstrip().upper().startswith("INSERT INTO") else [line],
				dumpStr.splitlines()
			)
			for lines in subItems 
		)

		with open(sqlFile, mode="w", encoding="utf-8") as sqlDumpFile:
			sqlDumpFile.writelines(dumpLines)


	def _separateInsertIntoMultilines(self, s):
		openBrackets = 0
		subStrStart = 0
		i = 0
		lastClosingBracketPos = s.rfind(')')
		strLen = len(s)
		withinStr = False
		strSign = None
		def checkForCh(ch, *compCh):
			nonlocal withinStr, s, i, strSign
			return (not withinStr or ch == strSign) and ch in compCh and (s[i-1] != '\\' or (s[i-1] == '\\' and s[i-2] == '\\'))

		while i < strLen:
			if checkForCh(s[i], '('):
				if (subStrStart < i):
					yield self._indention*openBrackets + s[subStrStart:i]
				yield self._indention*openBrackets + '('
				subStrStart = i+1
				openBrackets += 1
			elif checkForCh(s[i], ')'):
				if (lastClosingBracketPos == i):
					yield self._indention*openBrackets + s[subStrStart:i]
					openBrackets -= 1
					yield self._indention*openBrackets + s[i:]
					break
				elif (s[i+1] == ','):
					yield self._indention*openBrackets + s[subStrStart:i]
					openBrackets -= 1
					yield self._indention*openBrackets + '),'
					i += 1
					subStrStart = i + 1
				else:
					yield self._indention*openBrackets + s[subStrStart:i]
					openBrackets -= 1
					yield self._indention*openBrackets + ')'
					subStrStart = i + 1
			elif checkForCh(s[i], ','):
				yield self._indention*openBrackets + s[subStrStart:i+1]
				subStrStart = i+1
			else:
				if withinStr:
					if checkForCh(s[i], strSign):
						withinStr = False
						strSign = None
				else:
					if checkForCh(s[i], "'", '"'):
						withinStr = True
						strSign = s[i]
			i += 1

if __name__ == '__main__':
	try:
		mysql2dump = Mysql2Dump(Mysql2Dump.initializeConfig(pathlib.Path(__file__).parent.resolve()))
		if mysql2dump.returnCode > 0:
			exit(mysql2dump.returnCode)
		if mysql2dump.checkMode():
			exit(0)

		returnCode = 0
		with mysql2dump as dump:
			dump.run()
			returnCode = dump.returnCode

		exit(returnCode)
	except dh.DumpException as err:
		exit(*err.args)


