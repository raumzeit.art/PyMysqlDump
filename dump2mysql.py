import DumpHelper as dh
import mysql.connector
import subprocess
import platform
import os
from os import path
import sys
from threading import Thread
import pathlib

class Dump2MysqlBase(dh.DumpHelperBase):

	def __enter__(self):
		super().__enter__()
		if (self._cmdArgs["replacer"] == "intern"):
			self._intern_remote2LocalHost()
		self._prepare()
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self._cleanup()
		if (self._cmdArgs["replacer"] == "wp-cli"):
			self._wp_remote2LocalHost()
		else:
			self._intern_local2RemoteHost()
		super().__exit__(exc_type, exc_val, exc_tb)

	def dropObsoletTables(self):
		try:
			self._executeSQL("SET FOREIGN_KEY_CHECKS = 0")
			self._executeSQL(self._dropObsoletTablesSQL(), True)
			self._connection.commit()
		except mysql.connector.Error as e:
			print (
				"Unable to drop old tables from database {}: {}".format(self.database, e),
				file=sys.stderr
			)
			self._connection.rollback()
			self._returnCode = dh.DROP_OLD_TABLES_FAILED
		except:
			self._connection.rollback()
			raise

	def _dropObsoletTablesSQL(self):
		tables = {path.splitext(sqlFileName)[0] for sqlFileName in filter(
			lambda dirEntry : path.splitext(dirEntry)[1].lower() == ".sql",
			os.listdir(path=self._cmdArgs["location"])
		)}
		showTablesCursor = self._connection.cursor(buffered=True)
		showTablesCursor.execute("SHOW TABLES")
		existingTables = {table for (table,) in showTablesCursor}
		showTablesCursor.close()
		tablesToDelete = existingTables.difference(tables)
		if tablesToDelete:
			self._verbosePrint("Drop obsolete tables from database `{database}`:\n  - {0}".format("\n  - ".join(tablesToDelete), **self._cmdArgs))
		return "\n".join( ("DROP TABLE IF EXISTS `{}`;".format(table) for table in tablesToDelete) )

	def _prepare(self):
		self._restoreDumpCur = self._connection.cursor()
		self.dropObsoletTables()
	def _cleanup(self):
		try:
			self._executeSQL("SET FOREIGN_KEY_CHECKS = 1")
			self._connection.commit()
		except mysql.connector.Error as e:
			print (
				"Error during manipulating of the database {}: {}".format(self.database, e),
				file=sys.stderr
			)
			self._connection.rollback()
			self._returnCode = dh.CLEANUP_MYSQL_DB_FAILED
		except:
			self._connection.rollback()
			raise

		self._restoreDumpCur.close()

	def _executeSQL(self, sql, splitSql=False):
		if sql is None or sql == "":
			return

		def skipCommentLines(command):
			return "\n".join(filter(
				lambda line: line != "" and not line.startswith("--"), 
				map(lambda line: line.strip(), command.split("\n"))
			))

		if splitSql:
			for sqlCommand in map(skipCommentLines, sql.split(";")):
				result = self._restoreDumpCur.execute(sqlCommand, multi=True)
				if result is not None:
					result.send(None)
		else:
			result = self._restoreDumpCur.execute(sql, multi=True)
			if result is not None:
				result.send(None)

	def _addScriptParmeters(self, cmdParser):
		dump2MysqlCmdLineOptions = cmdParser.add_argument_group("dump2mysql options")
		self._addCmdlArg(dump2MysqlCmdLineOptions, "dump2mysql", "-r", "--replacer", choices=['wp-cli', 'intern'], default="wpi-cli", help="Set the replacement algorithm, which should be used for automatically host adjustmenst")
			


class DumpTabbed2Mysql(Dump2MysqlBase):

	def run(self):
		self._readDumpFiles()

	def _readDumpFiles(self):
		for fileName, sqlFile in self._get_sql_dump_files(True):
			self._loadDump(
				sqlFile = sqlFile, 
				txtFile = "{}.txt".format(path.splitext(sqlFile)[0]), 
				tableName = path.splitext(fileName)[0]
			)
			if self._returnCode > 0:
				break
	
	def _loadDump(self, *, sqlFile, txtFile, tableName):
		try:
			with open(sqlFile, mode="r", encoding="utf-8") as sqlFileHandler:
				self._executeSQL(sqlFileHandler.read(), True)

			self._executeSQL(
				"LOAD DATA LOCAL INFILE '{}' INTO TABLE {} CHARACTER SET utf8mb4 FIELDS TERMINATED BY '~@~'".format(
					txtFile, tableName
				)
			)

		except mysql.connector.Error as e:
			print ("Unable to create table from {} or to load dump {} into database {}: {}".format(sqlFile, txtFile, self.database, e), file=sys.stderr)
			self._returnCode = dh.LOADING_DUMP_FAILED
			self._connection.rollback()
		except OSError as e:
			print ("Unable to read sql file {}: {}".format(sqlFile, e), file=sys.stderr)
			self._returnCode = dh.READ_SQL_FILE_FAILED
			self._connection.rollback()
		except:
			self._connection.rollback()
			raise

class Dump2Mysql(Dump2MysqlBase):

	def run(self):
		self._readDumpFiles()

	def __enter__(self):
		super().__enter__()
		self._mysqlArgs= [
			"mysql",
			"--login-path={}".format(self._cmdArgs["login_path"]) if self._checkArg("login_path") else "",
			self.database
		]
		return self

	def _readDumpFiles(self):
		dumpFiles = list(self._get_sql_dump_files(True))
		for ignore, dirEntry in dumpFiles:
			self._handleDumpLoading(dirEntry)
			if self._returnCode > 0:
				break

		self._verbosePrint("Load {0} tables into the database `{database}`".format(len(dumpFiles), **self._cmdArgs))

	def _handleDumpLoading(self, sqlFile):
		procHandler = self._popen(*self._mysqlArgs)
		with open(sqlFile, mode="rb") as sqlDumpFile:
			mysqlStdout, mysqlStderr = procHandler.communicate(sqlDumpFile.read())

		if (procHandler.returncode > 0):
			print ("Unable to load dump {} into database {}: {}".format(sqlFile, self.database, mysqlStderr.decode()), file=sys.stderr)
			self._returnCode = dh.LOADING_DUMP_FAILED

if __name__ == '__main__':
	try:
		cnf = Dump2Mysql.initializeConfig(pathlib.Path(__file__).parent.resolve())
		if Dump2Mysql.useTabOption(cnf):
			dump2Mysql = DumpTabbed2Mysql(cnf)
		else:
			dump2Mysql = Dump2Mysql(cnf)

		if dump2Mysql.returnCode > 0:
			exit(dump2Mysql.returnCode)

		if dump2Mysql.checkMode():
			exit(0)

		returnCode = 0
		with dump2Mysql as dump:
			dump.run()
			returnCode = dump.returnCode

		exit(returnCode)
	except dh.DumpException as err:
		exit(*err.args)