import mysql.connector
import sys
import subprocess
from os import path
import os
from configparser import ConfigParser
import argparse
import math
import pathlib
from threading import Thread

#Error Codes
MISSING_CMD = 1
MISSING_CONFIGURATION = 2
DUMPING_TABLE_FAILED = 3
DROP_OLD_TABLES_FAILED = 4
LOADING_DUMP_FAILED = 5
READ_SQL_FILE_FAILED = 6
CLEANUP_MYSQL_DB_FAILED = 7
WORDPRESS_COMMAND_LINE_FAILED = 8

class DumpException(Exception):
	pass

class DumpHelperBase:
	configFile = "mysqlDump.cnf"
	def __init__(self, cnf):
		self.__cnf = cnf

		# Read version
		dumpHelperPath = pathlib.Path(__file__).parent.resolve()
		with open(os.path.join(dumpHelperPath, "version")) as fh:
			self._version = fh.readline()
		
		# Parse Command Line
		cmdParser = argparse.ArgumentParser(
			description='Mysql Dump Helper Scripts (Version {}). All parameters defined in the config file {} can be overwritten by the command line. The boolean setting `tab` in the section [dump] can only be set in the setting file. This setting switches on the --tab option for the dump. It is probably the fastest way to dump a mysql database, but it has several restrictions. Please consult the mysql documentation for more information'.format(self._version, self.configFile),
			formatter_class=argparse.ArgumentDefaultsHelpFormatter
		)
		cmdParser.add_argument("-V", "--version", action="store_true", help="Print the version")
		self._addCmdlArg(cmdParser, "config", "-v", "--verbose", action="count", default=0, help="Switch on verbos mode. Can be given several times")
		self._addCmdlArg(cmdParser, "config", "-t", "--threads", type=int, default=4, help="Number of parallel threads which are used for formating the sql dump output")
		cmdParser.add_argument("-c", "--check", action="store_true", default=False, help="Just check, if all requirements to run the script a satisfied and exit")
		self._addCmdlArg(cmdParser, "db", "-d", "--database", required="true", help="Set the name of the database to dump")
		self._addCmdlArg(cmdParser, "db", "--db-login", choices=['config', 'login-path'], default="login-path", help="Should the script use credentials from the login-path (which has been set by `mysql_config_editor` (only available for mysql)) or from the command line / configuration file") 
		self._addCmdlArg(cmdParser, "db", "--login-path", help="Mysql connection configuration. This information is read from ~/.mylogin.cnf")
		self._addCmdlArg(cmdParser, "dump", "-l", "--location", required="true", help="Dump location")
		
		dbLoginCnf = cmdParser.add_argument_group("database login configuration")
		self._addCmdlArg(dbLoginCnf, "db", "-H", "--host", help="Sets the host of mysql server")
		self._addCmdlArg(dbLoginCnf, "db", "-P", "--port", help="Sets the port of mysql server")
		self._addCmdlArg(dbLoginCnf, "db", "-u", "--user", help="Sets the user, which should be used to connect to the mysql server")
		self._addCmdlArg(dbLoginCnf, "db", "-p", "--password", help="Sets the password, which should be used to connect to the mysql server")

		wordpressCmdParser = cmdParser.add_argument_group("wordpress options")
		self._addCmdlArg(wordpressCmdParser, "wordpress", "-a", "--activate-host-adjustment", default=False, action="store_true", help="The host adjustment uses the wordpress command line (wp-cli) to automatically translate wordpress sites in the database between the configured remote-host and local-host, before and after mysql2dump.py and dump2mysql.py calls")
		self._addCmdlArg(wordpressCmdParser, "wordpress", "--wp-cli", default="wp", help="Wordpress command line tool")
		self._addCmdlArg(wordpressCmdParser, "wordpress", "--wp-location", help="If given, the script switches the working directory to this location before executing the wp-cli command")
		self._addCmdlArg(wordpressCmdParser, "wordpress", "-R", "--remote-host", help="Set the remote host of a wordpress site")
		self._addCmdlArg(wordpressCmdParser, "wordpress", "-L", "--local-host", help="Set the local host of a wordpress site")
		self._addScriptParmeters(cmdParser)

		self._cmdArgs = vars(cmdParser.parse_args())

		self._cmdArgs["tab"] = self.useTabOption(self.__cnf)

		if "database" not in self._cmdArgs or self._cmdArgs["database"] is None:
			print (
				"Please specify the database",
				file=sys.stderr
			)
			self._returnCode = MISSING_CONFIGURATION
			raise DumpException(self._returnCode)

		self.__database = self._cmdArgs["database"]

		self._returnCode = 0
		mysqlConnectionArgs = dict()
		if self._cmdArgs["db_login"] == "config":
			mysqlConnectionArgs = {key:self._cmdArgs[key] for key in ("user", "password", "host", "port") if self._checkArg(key) }
		else:
			import myloginpath
			mysqlConnectionArgs = myloginpath.parse(self._cmdArgs["login_path"])
		
		mysqlConnectionArgs["database"] = self.database

		self._connection = mysql.connector.connect(**mysqlConnectionArgs)

		self._checkMysql()
		if self._cmdArgs["activate_host_adjustment"]:
			if "remote_host" not in self._cmdArgs or "local_host" not in self._cmdArgs:
				print (
					"Please specify the remote-host and the local-host",
					file=sys.stderr
				)
				self._returnCode = MISSING_CONFIGURATION
				raise DumpException(self._returnCode)
			self._checkWpCli()

	def __enter__(self):
		self._threadList = []
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self._waitForThreads()

	def checkMode(self):
		if self._cmdArgs["version"]:
			print(self._version)
			return True

		if self._cmdArgs["check"]:
			if self._returnCode == 0:
				print ("Everything seems to be fine")
			return True
		else:
			return False
	def _checkArg(self, arg):
		return arg in self._cmdArgs and self._cmdArgs[arg] is not None

	def _verbosePrint(self, *args, **kwargs):
		if self._cmdArgs["verbose"] < 1 or self._returnCode > 0:
			return
		print(*args, **kwargs)

	def _veryVerbosePrint(self, *args, **kwargs):
		if self._cmdArgs["verbose"] < 2 or self._returnCode > 0:
			return
		print(*args, **kwargs)

	def _checkMysql(self):
		processResult = self._execute("mysql", "--version")
		if (processResult.returncode != 0):
			print (
				"There is a problem with your `mysql` command. Please check, if it is installed correctly:\n{}".format(processResult.stderr.decode()),
				file=sys.stderr
			)
			self._returnCode = MISSING_CMD
			raise DumpException(self._returnCode)

	def _checkWpCli(self):
		if self._cmdArgs["replacer"] != "wp-cli":
			return
		processResult = self._execute(self._cmdArgs["wp_cli"], "--info")
		if (processResult.returncode != 0):
			print (
				"There is a problem with your worpress commandline interface. Please check, if it is installed correctly:\n{}".format(processResult.stderr.decode()),
				file=sys.stderr
			)
			self._returnCode = MISSING_CMD
			raise DumpException(self._returnCode)

	def _addScriptParmeters(self, cmdParser):
		pass

	def _addCmdlArg(self, cmdParser, cat, *args, **kwargs):
		if len(args) == 2:
			shortName = args[0].lstrip("-")
			settingName = args[1].lstrip("-")
		else:
			shortName = None
			settingName = args[0].lstrip("-")
		kwargs["help"] = kwargs["help"] + ". This parameter can be also configured by using the setting `{}` in the section [{}].".format(settingName, cat)
		negatedKwargs = None
		negatedParameter = tuple()
		if "action" in kwargs and (kwargs["action"] == "store_true"):
			cmdParser = cmdParser.add_mutually_exclusive_group()
			if shortName is None:
				negatedParameter = ("--no-{}".format(settingName),)
			else:
				negatedParameter = ("-{}".format(shortName.upper()), "--no-{}".format(settingName))
			negatedKwargs = kwargs.copy()
			negatedKwargs["action"] = "store_false"
			negatedKwargs["dest"] = settingName.replace("-", "_")
		if cat in self.__cnf:
			if settingName in self.__cnf[cat]:
				if "action" in kwargs and (kwargs["action"] == "store_true"):
					kwargs["default"] = self.__cnf[cat].getboolean(settingName)
					negatedKwargs["default"] = kwargs["default"]
				elif ("action" in kwargs and kwargs["action"] == "count") or ("type" in kwargs and kwargs["type"] is int):
					kwargs["default"] = self.__cnf[cat].getint(settingName)
				else:
					kwargs["default"] = self.__cnf[cat][settingName]
				if "required" in kwargs:
					del kwargs["required"]
		cmdParser.add_argument(*args, **kwargs)
		if negatedParameter:
			cmdParser.add_argument(*negatedParameter, **negatedKwargs)

	def _get_sql_dump_files(self, bAddAbsolute = False):
		result = filter(
			lambda dirEntry : path.splitext(dirEntry)[1].lower() == ".sql",
			os.listdir(path=self._cmdArgs["location"])
		)
		if bAddAbsolute:
			return map(
				lambda dirEntry : (dirEntry, path.join(self._cmdArgs["location"], dirEntry)),
				result
			)
		else:
			return result 

	def _waitForThreads(self):
		for t in self._threadList:
			t.join()
		self._threadList = []

	def _wp_remote2LocalHost(self):
		if not self._checkArg("activate_host_adjustment") or not self._cmdArgs["activate_host_adjustment"]:
			return
		oldCWD = None
		if self._checkArg("wp_location"):
			oldCWD = os.getcwd()
			os.chdir(self._cmdArgs["wp_location"])

		self._verbosePrint("Translate Wordpress database from remote host ({remote_host}) to local host({local_host})".format(**self._cmdArgs))
		processResult = self._execute(self._cmdArgs["wp_cli"], "search-replace", self._cmdArgs["remote_host"], self._cmdArgs["local_host"])
		if oldCWD is not None:
			os.chdir(oldCWD)
		if processResult.returncode != 0:
			print(
				"Execution of `{wp_cli}` failed: {0}".format(processResult.stderr.decode(), **self._cmdArgs),
				file=sys.stderr
			)
			self._returnCode = WORDPRESS_COMMAND_LINE_FAILED
			raise DumpException(self._returnCode)
		self._veryVerbosePrint(processResult.stdout)

	def _wp_local2RemoteHost(self):
		if not self._checkArg("activate_host_adjustment") or not self._cmdArgs["activate_host_adjustment"]:
			return
		oldCWD = None
		if self._checkArg("wp_location"):
			oldCWD = os.getcwd()
			os.chdir(self._cmdArgs["wp_location"])
		
		self._verbosePrint("Translate Wordpress database from local host ({local_host}) to remote host ({remote_host})".format(**self._cmdArgs))
		processResult = self._execute(self._cmdArgs["wp_cli"], "search-replace", self._cmdArgs["local_host"], self._cmdArgs["remote_host"])
		if oldCWD is not None:
			os.chdir(oldCWD)
		if processResult.returncode != 0:
			print(
				"Execution of `{wp_cli}` failed: {0}".format(processResult.stderr.decode(), **self._cmdArgs),
				file=sys.stderr
			)
			self._returnCode = WORDPRESS_COMMAND_LINE_FAILED
			raise DumpException(self._returnCode)
		self._veryVerbosePrint(processResult.stdout)	


	def _intern_remote2LocalHost(self):
		if not self._checkArg("activate_host_adjustment") or not self._cmdArgs["activate_host_adjustment"]:
			return
		self._verbosePrint("Translate Wordpress dump files from remote host ({remote_host}) to local host({local_host})".format(**self._cmdArgs))
		sqlFiles, i_max, j_max = self.__get_replace_threadding_parameters()
		for i in range(0, self._cmdArgs["threads"]):		
			t = Thread(target = self.__intern_hostAdjust, args=(i, i_max, j_max, sqlFiles, self._cmdArgs["remote_host"], self._cmdArgs["local_host"]))
			t.start()
			self._threadList.append(t)
		self._waitForThreads()
		
	def _intern_local2RemoteHost(self):
		if not self._checkArg("activate_host_adjustment") or not self._cmdArgs["activate_host_adjustment"]:
			return
		self._verbosePrint("Translate Wordpress dump files from local host ({local_host}) to remote host ({remote_host})".format(**self._cmdArgs))
		sqlFiles, i_max, j_max = self.__get_replace_threadding_parameters()
		for i in range(0, self._cmdArgs["threads"]):		
			t = Thread(target = self.__intern_hostAdjust, args=(i, i_max, j_max, sqlFiles, self._cmdArgs["local_host"], self._cmdArgs["remote_host"]))
			t.start()
			self._threadList.append(t)
		self._waitForThreads()

	def __intern_hostAdjust(self, i, i_max, j_max, sqlFiles, fromHost, toHost):
		for j in range(0, j_max):
			index = j + i*j_max
			if index >= i_max:
				return
			sqlFile = sqlFiles[index]
			content = None
			with open(sqlFile, "r", encoding="utf-8") as fh:
				content = fh.read()
			with open(sqlFile, "w", encoding="utf-8") as fh:
				fh.write(content.replace(fromHost, toHost))

	def __get_replace_threadding_parameters(self):
		sqlFiles = list(map(
			lambda dir: dir[1],
			self._get_sql_dump_files(True)
		))
		i_max = len(sqlFiles)
		j_max = math.ceil(i_max/self._cmdArgs["threads"])
		return sqlFiles, i_max, j_max
	@staticmethod
	def _execute(*args):
		return subprocess.run(
			" ".join(args),
			shell=True,
			stdin=subprocess.PIPE, 
			stdout=subprocess.PIPE, 
			stderr=subprocess.PIPE
		)
	@staticmethod
	def _popen(*args):
		return subprocess.Popen(
			" ".join(args),
			shell=True,
			stdin=subprocess.PIPE, 
			stdout=subprocess.PIPE, 
			stderr=subprocess.PIPE
		)

	@staticmethod
	def initializeConfig(fileDir):
		cnfFile = path.join(fileDir, DumpHelperBase.configFile)
		cnf = ConfigParser()
		if not path.isfile(cnfFile):
			return cnf

		with open(cnfFile) as configFile:
			cnf.read_string(configFile.read())
		return cnf

	@staticmethod
	def useTabOption(cnf):
		if "dump" in cnf:
			if "tab" in cnf["dump"]:
				return cnf["dump"].getboolean("tab")
		return False

	@property
	def returnCode(self):
		return self._returnCode

	@property
	def database(self):
		return self.__database


