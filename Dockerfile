# syntax=docker/dockerfile:1

FROM ubuntu:latest AS PythonSystem
WORKDIR /usr/bin/
RUN apt-get update && \
  apt-get install -y python3 python3-distutils && \
  ln -s python3 python
RUN groupadd -r pyMysqlDump && \
  useradd --no-log-init -r -m -s /bin/bash -g pyMysqlDump pyMysqlDump


FROM PythonSystem AS PythonPip
RUN apt-get update && \
  apt-get install -y python3-apt
USER pyMysqlDump
WORKDIR /home/pyMysqlDump/pipInstaller
ADD --chown=pyMysqlDump:pyMysqlDump https://bootstrap.pypa.io/get-pip.py ./
RUN python get-pip.py



FROM PythonSystem AS PyMysqlDump
WORKDIR /usr/bin/
COPY --from=mysql:latest /usr/bin/mysql /usr/bin/mysqldump /usr/bin/mysql_config_editor ./

USER pyMysqlDump
WORKDIR /home/pyMysqlDump/.local/
COPY --from=PythonPip /home/pyMysqlDump/.local ./
ENV PATH=/home/pyMysqlDump/bin:/home/pyMysqlDump/.local/bin:${PATH}
RUN python -m pip install --upgrade pip && \
  pip install mysql-connector-python && \
  pip install myloginpath

WORKDIR /home/pyMysqlDump/bin

COPY --chown=pyMysqlDump:pyMysqlDump DumpHelper ./DumpHelper/
COPY --chown=pyMysqlDump:pyMysqlDump dump2mysql* mysql2dump* ./

WORKDIR /home/pyMysqlDump

ENTRYPOINT ["echo", "This is just a base image for raumzeit/mysql2dump:latest and raumzeit/dump2mysql:latest"]
