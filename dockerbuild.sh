#!/usr/bin/bash

VERSION=`cat DumpHelper/version`

docker build -t raumzeit/pymysqldump:${VERSION} -t raumzeit/pymysqldump:latest .
docker build -f ./Dockerfile.dump2mysql -t raumzeit/dump2mysql:${VERSION} -t raumzeit/dump2mysql:latest .
docker build -f ./Dockerfile.mysql2dump -t raumzeit/mysql2dump:${VERSION} -t raumzeit/mysql2dump:latest .