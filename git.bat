@echo off

:: Check, if the environment variables MYSQL_TO_DUMP and DUMP_TO_MYSQL are set.
:: if they are undefined, they are set to the default values "python ./scripts/mysql2dump.py"
:: and "python ./scripts/dump2mysql.py", respectively
if "%MYSQL_TO_DUMP%" == "" (
	set MYSQL_TO_DUMP="python ./scripts/mysql2dump.py"
)
if "%DUMP_TO_MYSQL%" == "" (
	set DUMP_TO_MYSQL="python ./scripts/dump2pysql.py"
)

:: Check, if environment variable MYSQL_DUMP is set. Otherwise, set it to the default "mysqldump"
:: MYSQL_DUMP should be set to the location, where the python script `mysql2dump.py` dumps its data,
:: but relative to the wordpress repository root.
if "%MYSQL_DUMP%" == "" (
	set MYSQL_DUMP="mysqldump"
)


:: Check, if environment variable GIT_COMMAND is set or automatically fetch the path to the git command
:: (must change directory, otherwise it would fetch the path of this script here)
if "%GIT_COMMAND%" == "" (
	set GIT_COMMAND=git.exe
)


:: Actual git script, which checks for specific git commands, if mysql2dump.py or dump2mysql.py must be
:: called, before or after the reald git command is called.
if "%1" == "pull" goto dump_to_mysql
if "%1" == "checkout" goto dump_to_mysql
if "%1" == "add" goto mysql_to_dump
if "%1" == "status" goto mysql_to_dump

goto fallback

:fallback
::Fallback - just call the normal git command
call %GIT_COMMAND% %*
exit /B %errorlevel%

:dump_to_mysql
for /F "tokens=* USEBACKQ" %%F in (`%GIT_COMMAND% %*`) do (
set GIT_COMMAND_OUT=%%F
)
set GIT_COMMAND_ERROR_CODE = %ERRORLEVEL%

echo %GIT_COMMAND_OUT%
if not "%GIT_COMMAND_OUT%" == "Already up to date." (		
	echo "Update mysql database..."
	call %DUMP_TO_MYSQL%
)

set GIT_COMMAND_OUT=
exit /B %GIT_COMMAND_ERROR_CODE%

:mysql_to_dump

if "%2" == "db" goto mysql_to_dump_only_db
if "%2" == "%MYSQL_DUMP%" goto mysql_to_dump_and_git_fallback
if "%2" == "." goto mysql_to_dump_and_git_fallback

goto fallback

:mysql_to_dump_only_db
echo "Update mysql dumps..."
call %MYSQL_TO_DUMP%
call %GIT_COMMAND% %1 %MYSQL_DUMP%
exit /B %errorlevel% 

:mysql_to_dump_and_git_fallback
echo "Update mysql dumps..."
call %MYSQL_TO_DUMP%
goto fallback