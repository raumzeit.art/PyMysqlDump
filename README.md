**Under construction...**

# PyMysqlDump
Small python scripts, which try to make a fast mysql dump (and load), whose ouput is optimized for git usage

# Install PyMysqlDump
The scripts are using python (so you need a python interpreter: see https://www.python.org). Additionally, you need two pacakges:

* mysql-connector-python (https://pypi.org/project/mysql-connector-python/)
* myloginpath (https://pypi.org/project/myloginpath/)

Please install them with the following commands:

```
pip install mysql-connector-python
pip install myloginpath
```

You need to create a `client.cnf` mysql configuration file, which stores the crediantials of an administrator account. These encrypted access information are used by the PyMysqlDump scripts to dump your database as well as load the database from an existing dump. To do this, please use the following command:

```
mysql_config_editor set --login-path=client --host=localhost --user=<USER> --password
```
_The script `mysql_config_editor` is located in the **bin** subdirectory of your mysql location_ 

Currently, this script is only working, if you execute the script on the host, where your mysql server is running. In addition, you must ensure, that your mysql server was called without the `--secure-file-priv` option. 

# mysqlDump.cnf

A lot of command line arguments can be also configured by the configuration file `mysqlDump.cnf`. The following sections and settings are available

    [config]
    # --threads parameter. Sets the maximum number of threads which can be used by the script
    threads = 8

    # --verbose parameter. Sets the verbose level. A higher verbose level gives you more output. Setting it to 0 switches it off.
    verbose = 1

    [db]
    # --databse parameter. Sets the database, which should be used
    database = wp

    # --db-login parameter. Possible values are {config,login-path}. Should the script use credentials from the login-path 
    # (which has been set by `mysql_config_editor` (only available for mysql)) or from the command line / configuration
    # file 
    db-login = login-path

    # --login-path parameter. Choose the login path configured in ~./mylogin.cnf. Default is `client`
    login-path = client
    
    [dump]
    # --location parameter. Sets the dump location
    location = <path/to/dump/directory>

    # no command line representation. This setting switches to --tab support of the mysqldump command
    # This is very special parameter, which supports high efficient dumping, but its also very limited
    # Please consult the mysql documentation
    # This setting is only in a beta state. Default is `no`
    tab = no

    [wordpress]
    # --activate-host-adjustment parameter. Switches the wordpress support on and off. Default is `no`
    activate-host-adjustment = yes
    
    # --wp-cli parameter. Sets the wp-cli command. Default is `wp`
    wp-cli = wp

    # --remote-host parameter. Sets the remote host of the wordpress page. Must be set, if 
    # activate-host-adjustment is set to `yes`
    remote-host = example-wp-page.com

    # --local-host parameter. Sets the local host of the wordpress page. Must be set, if 
    # activate-host-adjustment is set to `yes`
    local-host = 192.168.0.1

    # Specific commands for the dump2mysql script
    [dump2mysql]
    # --replacer  parameter. Possible values are {wp-cli,intern}. This command is used to replace the local-host by the remote-host and vice versa.
    # Normally, intern is the faster possibility and its better to maintain, because it doesn't need the wp-cli.
    replacer = intern

    # Specific commands for the mysql2dump script
    [mysql2dump]
    # --replacer  parameter. Possible values are {wp-cli,intern}. This command is used to replace the local-host by the remote-host and vice versa.
    # Normally, intern is the faster possibility and its better to maintain, because it doesn't need the wp-cli.
    replacer = intern
